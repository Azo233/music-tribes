from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Profile
from django import forms

class CreateUserForm(UserCreationForm):
    class Meta:
        model=User
        fields = ['username', 'email', 'password1', 'password2']

class LoginForm(ModelForm):
    class Meta:
        model=User
        password = forms.CharField(widget=forms.PasswordInput)

        widgets = {
            'password': forms.PasswordInput(),
        }

        fields = ['username', 'password']

        

class EditUserForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['image']
    
