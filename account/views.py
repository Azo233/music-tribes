from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from .Forms import CreateUserForm, LoginForm, EditUserForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout,update_session_auth_hash
from .models import Profile
from django.contrib.auth.forms import PasswordChangeForm


# Create your views here.

def registerView(request , *args, **kwargs):
    form = CreateUserForm() 

    if request.method == 'POST':
        form=CreateUserForm(request.POST)

        if form.is_valid():
            user=form.save()
            Profile.objects.create(
                user=user
            )
            messages.success(request, "Registracija uspijesna")
            return redirect('home')
            
    context= {'form':form}
    return render(request, "register.html",context)

def loginView(request, *args, **kwargs):
    form = LoginForm()
    if request.method=='POST':
        form=LoginForm(request.POST)
        username=request.POST.get('username')
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('home')
        else:
             messages.info(request, "Incorrect username or password")
    context={'form':form}
    return render(request, "login.html", context)

def logoutView(request, *args, **kwargs):
    logout(request)
    return redirect('login')

def profileView(request):
    user = get_object_or_404(User, id=request.user.id)

    if request.method == 'POST':   
        profile_form = EditUserForm(request.POST, request.FILES, instance=request.user.profile)
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('profile')
        else:
            messages.error(request, 'Password not changed!')

        if  profile_form.is_valid():
            profile_form.save()
            return redirect('home')
        else:
            messages.error(request, 'Picture not set.')
    else:
        profile_form = EditUserForm(instance=request.user.profile)
        form = PasswordChangeForm(request.user)

    
    return render(request, 'profile.html', {
        'profile_form': profile_form,
        'form': form,
        'picture':user.profile.image
    })