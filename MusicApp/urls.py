"""MusicApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from account.views import  registerView, loginView, logoutView, profileView
from playlist.views import playlist_detailView,create_playlistView,add_songView,delete_songView,playlist_editView, delete_playlistView, send_comment,Like,delete_comment
from tribe.views import Add_Tribe_View, homeView, tribe_detail_view,  join_tribe_view, leave_tribe_view, send_message,delete_message,kick_member


urlpatterns = [

    path('',homeView , name='home'),
    path('register',registerView , name='register'),
    path('login',loginView , name='login'),
    path('logout',logoutView , name='logout'),
    path('profile',profileView , name='profile'),


    path('add_tribe',Add_Tribe_View , name='add_tribe'),
    path('tribe_detail/<int:id>',tribe_detail_view , name='tribe_detail'),
    path('join_tribe/<int:id>', join_tribe_view, name='join_tribe'),
    path('leave_tribe/<int:id>', leave_tribe_view, name='leave_tribe'),
    path('send_message/<int:id>', send_message, name='send_message'),
    path('delete_message/<int:id>,<int:tribe_id>', delete_message, name='delete_message'),
    path('kick_member/<int:tribe_id>/<int:id>', kick_member, name='kick_member'),

    path('playlist_detail/<int:id>', playlist_detailView, name='playlist_detail'),
    path('create_playlist/<int:id>', create_playlistView, name='create_playlist'),
    path('add_song/<int:id>', add_songView, name='add_song'),
    path('delete_song/<int:id>/<int:chief_id>', delete_songView, name='delete_song'),
    path('playlist_edit/<int:id>', playlist_editView, name='playlist_edit'),
    path('delete_playlist/<int:id>', delete_playlistView, name='delete_playlist'),
    path('send_comment/<int:id><int:play_id>', send_comment, name='send_comment'),
    path('like/<int:id><int:play_id>',Like , name="like"),
    path('delete_comment/<int:id>/<int:chief_id>', delete_comment, name='delete_comment'),



    path('admin/', admin.site.urls),




]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
