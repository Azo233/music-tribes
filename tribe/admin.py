from django.contrib import admin
from .models import tribe, tribeJoin

# Register your models here.

admin.site.register(tribe)
admin.site.register(tribeJoin)