from django.shortcuts import render,redirect,get_object_or_404
from .models import Message, tribe, tribeJoin
from django.contrib import messages
from .forms import AddTribeForm, CreateMessageForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.urls import reverse
from playlist.models import playlist
from django.contrib.auth.decorators import login_required
# Create your views here.

def Add_Tribe_View(request):
    if request.method== 'POST':
        form=AddTribeForm(request.POST, request.FILES)
        
        if form.is_valid():
            Tribe=form.save(commit=False)
            Tribe.chieftain = request.user
            Tribe=form.save()
            return redirect('home')
        else:
            messages.error(request, ('Form is not filled properly'))
    else:
        form=AddTribeForm()
        context= {'form':form}
        return render(request, "add_tribe.html",context)


def homeView (request):
    all_tribes=tribe.objects.all()
    if request.user.is_authenticated:
        chief_tribes=[]
        for i in all_tribes:
            if i.chieftain == request.user:
                chief_tribes.append(i)
                
        not_chief_tribes=list(set(all_tribes) - set(chief_tribes))

        

        my_tribes_id=tribeJoin.objects.filter(user=request.user).values_list("tribe",flat=True)
        member_tribes=[]
        for id in my_tribes_id:
            member_tribes.append(get_object_or_404(tribe, pk=id))

        for i in not_chief_tribes:
            if request.user in member_tribes:
                member_tribes.append(i)
        
        not_member=list(set(not_chief_tribes) - set(member_tribes))
        return render (request, 'index.html', {
            'chief_tribes':chief_tribes,
            'member_tribes':member_tribes,
            'other_tribes':not_member
        })
    else:
        return render (request, 'index.html', {
            'other_tribes':all_tribes
        })

def tribe_detail_view (request,id):
    Tribe=get_object_or_404(tribe, pk=id)
    Form=CreateMessageForm()
    users=tribeJoin.objects.filter(tribe=Tribe)
    users_id=tribeJoin.objects.filter(tribe=Tribe).values_list("user",flat=True)
    PlayList=playlist.objects.filter(tribe=Tribe).values('playlistName','playlist_description','id')
    Messages=Message.objects.filter(mess_to=Tribe)
    users=[]
    for id in users_id:
        users.append(get_object_or_404(User, pk=id))
    context ={
        'users':users,
        'tribe':Tribe,
        'playlist':PlayList,
        'message_form':Form,
        'Messages':Messages
    }
    return render (request, 'tribe_details.html', context)

def join_tribe_view(request, id):
    Tribe=tribe.objects.get(pk=id)
    me=User.objects.get(id=request.user.id)
    join=tribeJoin(user=me, tribe=Tribe)
    join.save()
    return HttpResponseRedirect(reverse('tribe_detail', args=[id,]))


def leave_tribe_view(request, id): 
    context ={}
    Tribe=get_object_or_404(tribe, pk=id)
    obj = get_object_or_404(tribeJoin, tribe = Tribe, user = request.user) 
  
    if request.method =="POST": 
        
        obj.delete()
        return HttpResponseRedirect("/") 
  
    return render(request, "delete_view.html", context) 

def send_message(request, id):
    Tribe=get_object_or_404(tribe, pk=id)
    users=tribeJoin.objects.filter(tribe=Tribe).values('user')
    user_list=[]
    for i in users:
        user_list.append(i['user'])
    user_list.append(Tribe.chieftain.id)
    if request.user.id in user_list:
        if request.method== 'POST':
            message_form=CreateMessageForm(request.POST)
        
            if message_form.is_valid():
                NewMessage=message_form.save(commit=False)
                NewMessage.mess_to = get_object_or_404(tribe, pk=id)
                NewMessage.mess_from=request.user
                NewMessage=message_form.save()
            else:
                messages.success(request, ('not valid form!'))
        return HttpResponseRedirect(reverse('tribe_detail', args=[id,]))
    else:
        messages.info(request, ('You are not a member in this tribe!'))
        return HttpResponseRedirect(reverse('tribe_detail', args=[id,]))

@login_required(login_url="/login")
def delete_message(request, id, tribe_id):
    Tribe=get_object_or_404(tribe, pk=tribe_id)
    mess=get_object_or_404(Message, pk=id)
    
    if request.user == tribe.chieftain or request.user == mess.mess_from or request.user.is_staff:
        mess.delete()
    return HttpResponseRedirect(reverse('tribe_detail', args=[tribe_id,]))

@login_required(login_url="/login")
def kick_member(request, tribe_id,id): 
    context ={}
    name=get_object_or_404(User, pk=id)
    Tribe=get_object_or_404(tribe, pk=tribe_id)
    obj = get_object_or_404(tribeJoin, tribe = Tribe, user = name) 
    if request.user == Tribe.chieftain or request.user.is_staff:

        if request.method =="POST": 
            
            obj.delete()
            return HttpResponseRedirect(reverse('tribe_detail', args=[tribe_id,]))
  
        return render(request, "delete_view.html", context)
    else:
        messages.error(request, ("You are not chieftain of this tribe, you can't kick members out."))
        return HttpResponseRedirect(reverse('tribe_detail', args=[tribe_id,]))
