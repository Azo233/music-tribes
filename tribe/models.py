from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.utils import timezone
# Create your models here.

class tribe(models.Model):
    name=models.CharField(max_length=90)
    genre=models.CharField(max_length=90)
    image=models.ImageField(null=True, upload_to="TribeImages/")
    chieftain = models.ForeignKey(settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,blank=True,
    )
    users=models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="usersRel", through='tribeJoin')
    def __str__(self):
        return self.name

class tribeJoin(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, )
    tribe = models.ForeignKey(tribe,on_delete=models.CASCADE,)
    def __str__(self):
        return f"{self.user.username} joined on {self.tribe.name}"


class Message(models.Model):
    mess_from=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,blank=True,)
    mess_to=models.ForeignKey(tribe,on_delete=models.CASCADE,blank=True,)
    mess_content=models.TextField(max_length=200)
    time_sent=models.DateTimeField(default=timezone.now )
    def __str__(self):
        return self.mess_from.username