from .models import tribe,Message
from django.forms import ModelForm

class AddTribeForm(ModelForm):
    class Meta:
        model=tribe
        fields = ['name', 'genre', 'image']

class CreateMessageForm(ModelForm):
    class Meta:
        model = Message
        fields=['mess_content',]