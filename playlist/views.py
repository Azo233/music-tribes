from django.shortcuts import render, get_object_or_404,redirect
from .models import like, playlist, song,comment,like
from .forms import CreatePlaylistForm, AddSongForm, CreateCommentForm
from tribe.models import tribe, tribeJoin 
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


# Create your views here.

def playlist_detailView (request,id):

    if request.user.is_authenticated:
        tribe=get_object_or_404(playlist, pk=id).tribe
        comment_form=CreateCommentForm()
        Playlist=get_object_or_404(playlist, pk=id)
        likes_list=like.objects.filter(user=request.user).values('song')
        likes=[]
        for i in likes_list:
            likes.append(get_object_or_404(song, pk=i['song']))

        title_list=song.objects.filter(Playlist=Playlist)
        songs=[]
        for i in title_list:
            songs.append(i)
        all_comments=comment.objects.all()

        comments=[]
        for comm in all_comments:
            for sng in songs:
                if comm.song==sng:
                    comments.append(comm)
        
        context ={
            'playlist':Playlist,
            'songs':songs,
            'likes':likes,
            'comments':comments,
            'comment_form':comment_form,
            'tribe':tribe
        }
        return render (request, 'playlist.html', context)
    else:
        Playlist=get_object_or_404(playlist, pk=id)
        title_list=song.objects.filter(Playlist=Playlist)
        songs=[]
        for songy in title_list:
            songs.append(songy)
        all_comments=comment.objects.all()    
        comments=[]
        for comm in all_comments:
            for sng in songs:
                if comm.song==sng:
                    comments.append(comm)

        context ={
            'playlist':Playlist,
            'songs':songs,
            'comments':comments,
        }
        return render (request, 'playlist_guest.html', context)


@login_required(login_url="/login")
def create_playlistView (request,id):
    Tribe=get_object_or_404(tribe, pk=id)
    if request.user == Tribe.chieftain:
        if request.method== 'POST':
            form=CreatePlaylistForm(request.POST)
            
            if form.is_valid():
                NewPlaylist=form.save(commit=False)
                NewPlaylist.tribe = get_object_or_404(tribe, pk=id)
                NewPlaylist=form.save()
                return HttpResponseRedirect(reverse('playlist_detail', args=[NewPlaylist.id,]))

        else:
            form=CreatePlaylistForm()
            context= {'create_form':form}
            return render(request, "CreatePlaylist.html",context)
    else:
        messages.error(request, ('Only chieftain can create new Playlist.'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))

@login_required(login_url="/login")
def add_songView (request, id):
    Tribe=get_object_or_404(playlist, pk=id).tribe
    users=tribeJoin.objects.filter(tribe=Tribe).values('user')
    user_list=[]
    for i in users:
        user_list.append(i['user'])
    user_list.append(Tribe.chieftain.id)
    if request.user.id in user_list:
        if request.method== 'POST':
            form=AddSongForm(request.POST)
            
            if form.is_valid():
                NewSong=form.save(commit=False)
                NewSong.Playlist = get_object_or_404(playlist, pk=id)
                NewSong.user_added=request.user
                NewSong=form.save()
                return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))

        else:
            form=AddSongForm()
            context= {'form':form}
            return render(request, "AddSong.html",context)
    else: 
        messages.info(request, ('You are not a member in this tribe, you cannot add songs! Join tribe to add songs'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))


@login_required(login_url="/login")
def delete_songView (request, id, chief_id):
    
    obj = get_object_or_404(song, id = id)
    context ={}
    if request.user == obj.user_added or request.user.id == chief_id:
        if request.method =="POST": 
            
            obj.delete()
            return HttpResponseRedirect("/") 

        return render(request, "delete_view.html", context)
    else:
        messages.info(request, ('Only chieftain and member who added the song can delete it :('))
        return HttpResponseRedirect('/')

@login_required(login_url="/login")
def playlist_editView  (request, id):
    t=get_object_or_404(playlist, pk=id).tribe
    if request.user == t.chieftain:
        thisplaylist=get_object_or_404(playlist, id=id)
        if request.method == 'POST':
            
            playlist_form = CreatePlaylistForm(request.POST, instance=thisplaylist)
            if  playlist_form.is_valid():
                playlist_form.save()
                messages.success(request, ('Your playlist was successfully updated!'))
                return redirect('home')
            else:
                messages.error(request, ('Please correct the error below.'))
        else:
            playlist_form = CreatePlaylistForm(instance=thisplaylist)
            
        return render(request, 'CreatePlaylist.html', {
            'form': playlist_form
        })
    else:
        messages.info(request, ('Only chieftain can edit playlist info! You can ask him in chat :)'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))

@login_required(login_url="/login")
def delete_playlistView (request, id):
    t=get_object_or_404(playlist, pk=id).tribe
    if request.user == t.chieftain: 
        context ={} 
        obj = get_object_or_404(playlist, id = id) 
    
        if request.method =="POST": 
            
            obj.delete()
            return HttpResponseRedirect("/") 
    
        return render(request, "delete_view.html", context)
    else:
        messages.info(request, ('Only chieftain can delete playlists! You can ask him in chat :)'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[id,]))

login_required(login_url="/login")
def send_comment(request, id, play_id):
    Tribe1=get_object_or_404(playlist, pk=play_id).tribe
    users=tribeJoin.objects.filter(tribe=Tribe1).values('user')
    user_list=[]
    for i in users:
        user_list.append(i['user'])
    user_list.append(Tribe1.chieftain.id)
    if request.user.id in user_list:
        if request.method== 'POST':
            comment_form=CreateCommentForm(request.POST)
        
            if comment_form.is_valid():
                NewComment=comment_form.save(commit=False)
                NewComment.song = get_object_or_404(song, pk=id)
                NewComment.user=request.user
                NewComment.song.num_comments=NewComment.song.num_comments+1
                NewComment=comment_form.save()
                NewComment.song.save()
            else:
                messages.success(request, ('not valid form!'))
        else:
            messages.success(request, ('something else!'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))
    else:
        messages.info(request, ('You are not a member in this tribe!'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))

@login_required(login_url="/login")
def Like(request, id, play_id):
    if request.method =="POST": 
        Tribe1=get_object_or_404(playlist, pk=play_id).tribe
        users=tribeJoin.objects.filter(tribe=Tribe1).values('user')
        user_list=[]
        for i in users:
            user_list.append(i['user'])
        user_list.append(Tribe1.chieftain.id)
        if request.user.id in user_list:    
            Song = get_object_or_404(song, pk=id)
            if like.objects.filter(user=request.user, song=Song).exists():
                obj = get_object_or_404(like, user=request.user, song=Song) 
                obj.delete()
                Song.num_likes=Song.num_likes-1
                Song.save()
            else:
                newLike=like(song=Song, user= request.user)
                newLike.save()
                Song.num_likes=Song.num_likes+1
                Song.save()

            return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))
        else:
            messages.info(request, ('Only members can like songs!'))
            return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))
    else:
        messages.info(request, ('You can like songs only by clicking like button!'))
        return HttpResponseRedirect(reverse('playlist_detail', args=[play_id,]))


@login_required(login_url="/login")
def delete_comment(request, id, chief_id):
    
    obj = get_object_or_404(comment, id = id)
    context ={}
    if request.user == obj.user or request.user.id == chief_id or request.user.is_staff:
        if request.method =="POST": 
            
            obj.delete()
            return HttpResponseRedirect("/") 

        return render(request, "delete_view.html", context)
    else:
        messages.info(request, ('Only chieftain and member who added the comment can delete it :('))
        return HttpResponseRedirect('/')