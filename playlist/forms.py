from django.forms import ModelForm
from .models import playlist,song,comment

class CreatePlaylistForm(ModelForm):
    class Meta:
        model = playlist
        fields=['playlistName', 'playlist_description',]

class AddSongForm(ModelForm):
    class Meta:
        model = song
        fields=['trackArtist', 'trackTitle', 'youtube_url', 'duration',]


        
class CreateCommentForm(ModelForm):
    class Meta:
        model = comment
        fields=['text',]