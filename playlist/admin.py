from django.contrib import admin
from .models import song, playlist, comment

# Register your models here.

admin.site.register(song)
admin.site.register(playlist)
admin.site.register(comment)
