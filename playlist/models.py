from django.db import models

# Create your models here.
from django.db.models.deletion import CASCADE
from django.db.models.fields import BooleanField
from django.db.models.fields.related import ForeignKey
from tribe.models import tribe
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class playlist(models.Model):
    playlistName=models.TextField(max_length=50)
    playlist_description=models.TextField(max_length=200)
    tribe = models.ForeignKey(tribe,on_delete=models.CASCADE,)
    time_created= models.DateTimeField(default=timezone.now )

    def __str__(self):
            return self.playlistName

class song(models.Model):
    trackArtist=models.TextField(max_length=100)
    trackTitle=models.TextField(max_length=100)
    youtube_url=models.TextField(max_length=120)
    duration=models.DurationField()
    num_likes=models.IntegerField(default=0)
    num_comments=models.IntegerField(default=0)
    Playlist = models.ForeignKey(playlist, on_delete=models.CASCADE,)
    time_created=models.DateTimeField(default=timezone.now )
    user_added=models.ForeignKey(User, on_delete=models.CASCADE, blank=True)

    def __str__(self):
            return self.trackTitle

class comment(models.Model):
    song=models.ForeignKey(song,blank=True, related_name="comments", on_delete=models.CASCADE)
    user=models.ForeignKey(User,blank=True, on_delete=models.CASCADE)
    text=models.TextField()
    date_added=models.DateTimeField(default=timezone.now )

    def __str__(self):
        return self.user.username

class like(models.Model):
    song=models.ForeignKey(song, on_delete=models.CASCADE)
    user=models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        constraints=[
            models.UniqueConstraint(name='user_vote', fields = ['song','user'])
        ]
    def __str__(self):
        return f"{self.user.username} voted on {self.song.trackTitle}"

